//comentario de  1 linha
/* bloco de comentario*/
console.log("Repeticao WHILE");
let numero  = 0;
while(numero<=10){
    //console.log(`Valor nr: ${(numero)}`);
    if(numero%2==0){
        console.log(`Valor nr: ${(numero)} é par`);
    }else{
        console.log(`Valor nr: ${(numero)} é impar`)
    }
    numero++;
}
console.log("repeticao DO/WHILE");
let numero1 = 0;
do{
    console.log(`Valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=10);

console.log("repeticao FOR");
for(let numero2=0; numero2<=10;numero2++){
    console.log(`Valor nr: ${(numero2)}`);
}